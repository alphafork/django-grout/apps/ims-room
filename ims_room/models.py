from django.db import models
from ims_base.models import AbstractBaseDetail


class RoomType(AbstractBaseDetail):
    pass


class Room(AbstractBaseDetail):
    type = models.ForeignKey(RoomType, on_delete=models.CASCADE, null=True, blank=True)
